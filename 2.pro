domains
	lst=integer*
predicates
	prop(integer,string)			%�������� ��������
	objects(integer,string,lst)		%������� ���������� �������
	qv(integer,lst,lst)			%������� ������������ ��������
	next(char,lst,lst,integer,integer)	%���������� ������ ������������ �� �������� ������� 
	ex_in_lst(integer,lst)			%�������� �� ������� �������� � ������
	rvn(lst,lst)				%�������� ������������, ��� ��� �������� 1-�� ������ ���������� �� ������
	nrvn(lst,lst)				%�������������� ����������� ���������
	fst_in_lst(lst,lst,integer)		%���������� ������� �� ����������������� ��� �������� �������� ��������
	max(integer)				%���������, �������� �� ����� �������� ������������
	count(integer)				%������� ����� ��������, ������� ���������� � �� 
clauses
	%�������� ��������
	prop(1,"������ fb2").
	prop(2,"������ pdf").
	prop(3,"������ epub").
	prop(4,"������������� ���� ������").
	prop(5,"������������� �������� ����� ��������").
	prop(6,"������������ ���������� ������").
	prop(7,"��������� ����������").
	prop(8,"������� ����������� �������� ����").
	prop(9,"��������� ������ ������ iOS").

	%������� ���������� �������
	objects( 1, "uBooks xl",	[1,2,3,4,6,9]).
	objects( 2, "ShortBook",	[1,4,6,9]).
	objects( 3, "���������",	[1,7,8,9]).
	objects( 4, "Bookmate",		[1,3,7,8,9]).
	objects( 5, "Kobo",		[1,3,6,7,8]).
	objects( 6, "Bluefire Reader",	[1,3,4,6,8,8,9]).
	objects( 7, "ShuBook SE",	[1,2,3,4,5,6,8,9]).
	objects( 8, "Exlibris FB2",	[1,3,5,6,7,8]).
	objects( 9, "iBooks",		[2,3,7,8]).
	objects(10, "Marvin for iOS",	[3,4,5,6,8,9]).

	%������� ������������ ��������
	qv(N,T1,_):-
		objects(N,Name,L),
		rvn(T1,L),rvn(L,T1),write("�����: ��� �������� - ", Name),nl,!.
	qv(N,T1,T2):-
		objects(N,_,L), 
		rvn(T1,L), 
		nrvn(T2,L),
		fst_in_lst(L,T1,N2),prop(N2,S),
		write("������: ",S,"? (y/n) - "), 
		readchar(Ch),
		next(Ch,T1,T2,N,N2).
	qv(N,T1,T2):-
		N1=N+1,
		qv(N1,T1,T2).
		qv(X,_,_):-
		count(X),write("���������� ���� �� �������"),nl,!.

	%���������� ������ ������������ �� �������� ������� 
	next('y',T1,T2,N,N1):-
		write("y"),nl, 
		qv(N,[N1|T1],T2),!.
	next('n',T1,T2,N,N1):-
		write("n"),nl, 
		N2=N+1, 
		qv(N2,T1,[N1|T2]),!.
	next(_,T1,T2,N,N1):-
		readchar(Ch),
		next(Ch,T1,T2,N,N1).

	%�������� �� ������� �������� � ������
	ex_in_lst(_,[]):-
		!,fail.
	ex_in_lst(X,[X|_]):-
		!.
	ex_in_lst(X,[_|T]):-
		ex_in_lst(X,T).

	%�������� ������������, ��� ��� �������� 1-�� ������ ���������� �� ������
	rvn([],_):-
		!.
	rvn([H|T],L):-
		ex_in_lst(H,L),
		rvn(T,L).

	%�������������� ����������� ���������
	nrvn([],_):-
		!.
	nrvn([H|T],L):-
		not(ex_in_lst(H,L)),
		nrvn(T,L).

	%���������� ������� �� ����������������� ��� �������� �������� ��������
	fst_in_lst([H|_],L,Rez):-
		not(ex_in_lst(H,L)),
		Rez=H.
	fst_in_lst([H|T],L,Rez):-
		ex_in_lst(H,L),
		fst_in_lst(T,L,Rez).

	%������� ����� ��������, ������� ���������� � �� 
	count(X):-
		objects(X,_,_),
		not(max(X)).
	max(X):-
		objects(Y,_,_),
		X<Y.
goal
	qv(1,[],[]).
