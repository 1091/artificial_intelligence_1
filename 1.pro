predicates
	positive(string,string)		%������������� �����
	negative(string,string)		%������������� �����
	ask(string,string)		%������ � ������������
	remember(string,string,char)	%����������� ������
	bookReader(string)			%����� �������
	
facts - general
	xpositive(string, string)
	xnegative(string, string)
  
clauses
	%������������� �����
	positive(X, Y):-
		xpositive(X,Y),
		!.
	positive(X, Y):-
		not(negative(X,Y)), 
		!, 
		ask(X,Y).

	%������������� �����
	negative(X, Y):-
		xnegative(X,Y),
		!.
  
	%������ � ������������
	ask(X, Y):-
		write("������: ",X," - ",Y,"? (y/n) - "),
		readchar(Ch),
		remember(X, Y, Ch).
    
	%����������� ������
	remember(X, Y, 'y'):-
		write("y"), 
		nl, 
		assertz(xpositive(X, Y)), 
		!.
	remember(X, Y, 'n'):-
		write("n"), 
		nl, 
		assertz(xnegative(X, Y)), 
		!, 
		fail.
	remember(X,Y,_):-
		readchar(Ch),
		remember(X, Y, Ch),
		!.

	% ����� �������
	% 1
	bookReader("uBooks xl"):-
		positive("������ fb2","������������"),
		positive("������ pdf","������������"),
		positive("������ epub","������������"),
		positive("���� ������","�������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������"),
		positive("���������� ������� ����","�����������"),
		positive("��������� ������ ������ iOS","����").	
	% 2
	bookReader("ShortBook"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","�� ������������"),
		positive("���� ������","�������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������"),
		positive("���������� ������� ����","�����������"),
		positive("��������� ������ ������ iOS","����").
	% 3
	bookReader("���������"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","�� ������������"),
		positive("���� ������","�� �������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�� �����������"),
		positive("���������","����������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","����").
	% 4
	bookReader("Bookmate"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","������������"),
		positive("���� ������","�� �������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�� �����������"),
		positive("���������","�������������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","����").
	% 5
	bookReader("Kobo"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","������������"),
		positive("���� ������","�� �������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�����������"),
		positive("���������","����������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","���").
	% 6
	bookReader("Bluefire Reader"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","������������"),
		positive("���� ������","�������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","����").
	% 7
	bookReader("ShuBook SE"):-
		positive("������ fb2","������������"),
		positive("������ pdf","������������"),
		positive("������ epub","������������"),
		positive("���� ������","�������������"),
		positive("�������� ����� ��������","�������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","����").
	% 8
	bookReader("Exlibris FB2"):-
		positive("������ fb2","������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","������������"),
		positive("���� ������","�� �������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������"),
		positive("���������� ������� ����","�����������"),
		positive("��������� ������ ������ iOS","����").
	
	% 9
	bookReader("iBooks"):-
		positive("������ fb2","�� ������������"),
		positive("������ pdf","������������"),
		positive("������ epub","������������"),
		positive("���� ������","�� �������������"),
		positive("�������� ����� ��������","�� �������������"),
		positive("���������� ������","�� �����������"),
		positive("���������","�������������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","���").
	% 10
	bookReader("Marvin for iOS"):-
		positive("������ fb2","�� ������������"),
		positive("������ pdf","�� ������������"),
		positive("������ epub","������������"),
		positive("���� ������","�������������"),
		positive("�������� ����� ��������","�������������"),
		positive("���������� ������","�����������"),
		positive("���������","�������"),
		positive("���������� ������� ����","����"),
		positive("��������� ������ ������ iOS","����").	
	% else
	bookReader("������ �� �������").
	
goal 
	bookReader(Book_reader), !.